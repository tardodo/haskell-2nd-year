import Data.List
import Data.Char
import qualified Data.Map as Map
import Data.Maybe
import Control.Monad (replicateM)

product' :: [Int] -> Int
product' [] = 0
product' xs = foldr (*) 1 xs

max' :: [Int] -> Int
max' [] = 0
max' xs = foldr (\x y -> if x > y then x else y) (minBound :: Int) xs

min' :: [Int] -> Int
min' [] = 0
min' xs = foldr (\x y -> if x < y then x else y) (maxBound :: Int) xs

card :: [Int] -> Map.Map Int Int
card [] = Map.empty
card xs = 
    let newList = [(x, 1) | x <- xs]
    in Map.fromListWith (+) newList

card_f :: [Int] -> Map.Map Int Int
card_f [] = Map.empty
card_f s@(x:xs) =
    let card' (y:ys) = 
            let occur = foldr (\z -> if y==z then (+1) else id) 0 s
            in if null ys then [] else (y, occur): card' ys 
    in Map.fromList(card' (x:xs))

mode :: [Int] -> [Int]
mode [] = [0]
mode xs =
    let mapped = card xs
        topOccur = maximum (Map.elems mapped)
        allKeys =  Map.keys mapped
    in  [x | x <- allKeys, mapped Map.! x == topOccur]

data Inst = LOAD_CONST Const 
    | LOAD_GLOBAL Var 
    | STORE_GLOBAL Var 
    | CALL_FUNCTION 
    | JUMP PC
    | JUMP_IF_FALSE PC 
    | ADD Var
    deriving (Show, Eq, Ord)

type BytecodeProgram = [Inst]
type PC = Int
type Globals = Map.Map Var Const
type Var = String
data Const = Num Integer | Prog BytecodeProgram deriving (Show, Eq, Ord)

instance Num Const where
    (+) (Num x) (Num y) = Num(x + y)
    (-) (Num x) (Num y) = Num(x - y)
    fromInteger x = Num x
    
class Executable a where
    exec' :: a -> (Globals, PC)  -> Maybe (Globals, PC)

instance Executable Inst where
    exec' (LOAD_CONST  c) (g, p) = Just (Map.insert "acc" c g, p+1)
    exec' (LOAD_GLOBAL v) (g, p) = 
        let value = g Map.!? v 
        in if isJust value then Just (Map.insert "acc" (fromJust value) g , p+1)
        else Nothing
    exec' (STORE_GLOBAL v) (g, p) = 
        let value = g Map.!? "acc"
        in if isJust value then Just (Map.insert v (fromJust value) g, p+1)
        else Nothing
    exec' (CALL_FUNCTION) (g, p) =
        let value = g Map.!? "acc"
            program = if isProg (value) && value /= Nothing then toProg (fromJust value) else []
            updatedGlobals = (Map.insert "acc" 0 g)
            newGlobals = if null program then Nothing else simpleExec' program updatedGlobals
        in if newGlobals == Nothing then Nothing else Just (fromJust newGlobals, p+1)
    exec' (JUMP pc) (g, p) = 
        if pc >= 0 then Just (g, pc) else Nothing
    exec' (JUMP_IF_FALSE pc) (g, p) = 
        let value = g Map.!? "acc"
        in if value == Just 0 && pc >= 0 then Just (g, pc)
        else Nothing   
    exec' (ADD v) (g, p) = 
        let accVal = g Map.!? "acc"
            varVal = g Map.!? v
        in if isJust accVal && isJust varVal && 
            isProg (accVal) == False && isProg (varVal) == False
        then Just (Map.insert "acc" ((fromJust accVal)+(fromJust varVal)) g, p+1)
        else Nothing
  
simpleExec' :: BytecodeProgram -> Globals -> Maybe Globals
simpleExec' [] global = Just global 
simpleExec' xs global =
    let executing _ [] g _ = Just g
        executing fullL (y:ys) g pc = 
            let executed = exec' y (g, pc)
                justExec = if executed == Nothing then (Map.empty,0) else fromJust executed
                toBeExec = goTo fullL (snd justExec) 
            in if executed == Nothing then Nothing 
            else executing fullL toBeExec (fst justExec) (snd justExec)
    in executing xs xs global 0

isProg :: Maybe Const -> Bool
isProg (Just (Prog a)) = True
isProg _ = False

toProg :: Const -> BytecodeProgram
toProg (Prog a) = a

goTo :: BytecodeProgram -> PC -> BytecodeProgram
goTo [] _ = []
goTo xs pc = drop pc xs

superOptimize :: [(Globals, Globals)] -> Int -> [Const] -> Maybe BytecodeProgram
superOptimize [] _ _ = Nothing
superOptimize global len const =
    let load_c = instrConst const
        allVarInst = instrVar global
        jump = [JUMP x | x <- [1..len]]
        jumpIfFalse = [JUMP_IF_FALSE x | x <- [1..len]]
        allInst = (CALL_FUNCTION) : (load_c ++ allVarInst ++ jump ++ jumpIfFalse)
        allCombs = replicateM len allInst
        noBackJumpCombs = [x | x<- allCombs, checkJumps x]
        result = [(x, simpleExec' x (fst y)) | x<-noBackJumpCombs, y <- global]
        validInstPair = filter (\ x -> (snd x) /= Nothing) result
        checkForAcc = foldr (&&) True [Map.member "acc" (fst x) | x <- global]
        newGlob = if checkForAcc then [(fst x,fromJust (snd x)) | x <- validInstPair]
             else [(fst x, Map.delete "acc" (fromJust (snd x))) | x <- validInstPair]
    in safeHead [fst x | x<-newGlob, checkGlobals x global]

checkGlobals :: (BytecodeProgram, Globals) -> [(Globals, Globals)] -> Bool
checkGlobals ([], _) _ = False
checkGlobals programPair global =
    let boolList = [Map.isSubmapOf (snd programPair) (snd x) | x <- global]
    in foldr (&&) True boolList

instrConst :: [Const] -> BytecodeProgram
instrConst [] = []
instrConst xs = [(LOAD_CONST x) | x <- xs]

instrVar :: [(Globals, Globals)] -> BytecodeProgram
instrVar [] = []
instrVar (x:xs) = 
    let inVars = Map.keys (fst x)
        load_g = [LOAD_GLOBAL x | x <- inVars]
        store_g = [STORE_GLOBAL x | x <- inVars]
        add = [ADD x | x<- inVars]
        allInst = load_g ++ store_g ++ add 
    in allInst++(instrVar xs)
 
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:xs) = Just x

checkJumps :: BytecodeProgram -> Bool
checkJumps [] = False
checkJumps xs =
    let anyJumps = [(x, filterJumps x xs) | x<- xs]
    in fst (Map.mapAccum (\x y -> (x && y, y && True)) True (Map.fromList anyJumps)) 

getValJ :: Inst -> Int
getValJ (JUMP a) = a
getValJ (JUMP_IF_FALSE a) = a

filterJumps :: Inst -> BytecodeProgram -> Bool
filterJumps (JUMP a) xs = if length(takeWhile (/=(JUMP a)) xs) < getValJ (JUMP a) && ((card_I xs) Map.! (JUMP a)) == 1 then True else False
filterJumps (JUMP_IF_FALSE a) xs = if length(takeWhile (/=(JUMP_IF_FALSE a)) xs) < getValJ (JUMP_IF_FALSE a) && ((card_I xs) Map.! (JUMP_IF_FALSE a)) == 1 then True else False
filterJumps _ xs = True

card_I :: [Inst] -> Map.Map Inst Int
card_I xs = 
    let newList = [(x, 1) | x <- xs]
    in Map.fromListWith (+) newList

testJump = checkJumps [LOAD_CONST 5, JUMP 1, ADD "v", JUMP 3]
testJump2 = checkJumps [JUMP 1, JUMP 1]
--testJump3 = checkJumps1 [LOAD_CONST 5, JUMP 2, ADD "v", JUMP 2]

testSuper = superOptimize [((Map.fromList [("x", 2), ("y", 3), ("z", 0)]),(Map.fromList [("x", 5), ("y", 3), ("z", 20)]))] 6 [2, 3, 5, 20, 0] -- 4:20
testSuper2 = superOptimize [(Map.fromList([("acc",  3), ("y", 0), ("z", 0)]), Map.fromList([("acc",  3), ("y", 3), ("z", 5)]))] 5 [(Prog [LOAD_CONST 5])]
testSuper3 = superOptimize [((Map.fromList [("acc", 0), ("x", 2), ("y", 3), ("z", 0)]),(Map.fromList [("acc", 3), ("x", 5), ("y", 3), ("z", 20)]))] 5 [3, 5, 20]
testSuper4 = superOptimize [((Map.fromList [("x", 2), ("y", 3), ("z", 0)]),(Map.fromList [("x", 3), ("y", 5), ("z", 20)]))] 6 [3, 5, 20] --4 mins
testSuper5 = superOptimize [(Map.fromList([("x", 3), ("y", 0), ("z", 0)]), Map.fromList([("x", 3), ("y", 3), ("z", 5)]))] 6 [(Prog [LOAD_CONST 5])] --1:50
testSuper6 = superOptimize [((Map.fromList [("x", 2), ("y", 3), ("acc", 0)]),(Map.fromList [("x", 5), ("y", 3), ("acc", 5)]))] 4 [2, 3, 5]
testSuper7 = superOptimize [((Map.fromList [("x", 0), ("y", 1), ("z", 2)]),(Map.fromList [("x", 1), ("y", 2), ("z", 2)])), 
    ((Map.fromList [("x", 0),("y", 3),("z", 2)]),(Map.fromList [("x", 3),("y", 3),("z", 2)]))] 6 [1, 2, 3]

testVars = instrVar [((Map.fromList [("x", 2), ("y", 3)]),(Map.fromList [("x", 5), ("y", 3)])), ((Map.fromList [("z", 2)]),(Map.fromList [("z", 5)]))]

loadComb = instrConst [5, 4, Prog [LOAD_CONST 7, STORE_GLOBAL "v"], 10]
test = simpleExec' [LOAD_CONST 5, LOAD_CONST (Prog [LOAD_CONST 10, STORE_GLOBAL "x"]), 
                CALL_FUNCTION, ADD "x"] (Map.empty)

test2 = simpleExec' [LOAD_CONST 5, LOAD_CONST (Prog [JUMP 2, STORE_GLOBAL "x", LOAD_CONST 7]), CALL_FUNCTION, JUMP 5, STORE_GLOBAL "y", LOAD_CONST 9] (Map.empty)
test3 = simpleExec' [LOAD_CONST 5, LOAD_CONST (Prog [CALL_FUNCTION, STORE_GLOBAL "x", LOAD_CONST 7]), CALL_FUNCTION] (Map.empty)
test4 = simpleExec' [LOAD_CONST 5, LOAD_CONST (Prog [STORE_GLOBAL "x"]), CALL_FUNCTION] (Map.empty)
test5 = simpleExec' [LOAD_CONST (Prog [LOAD_CONST 5]), ADD "x"] (Map.fromList [("x", 10)])
test6 = simpleExec' [LOAD_CONST 10, CALL_FUNCTION, LOAD_CONST 20] Map.empty
test7 = simpleExec' [LOAD_CONST 9, JUMP 0] Map.empty
test8 = simpleExec' [JUMP 1, JUMP 1] Map.empty

--((Map.fromList [("z", 2)]),(Map.fromList [("z", 5)]))
